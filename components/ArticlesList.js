import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import ArticleComponent from './ArticleComponent'
import ErrorMessage from './ErrorMessage'

export default class ArticlesList extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.loadArticles()
  }

  render() {
    const {articles, articlesError} = this.props

    if(articlesError) {
        return <ErrorMessage message={articlesError} />
    }

    if(!articles) {
        { return <div className="loading">Loading article...</div> }
    }

    return (
        <div className="articles-list">
          <h1>Articles</h1>
          {articles.map(item => (
            <ArticleComponent key={`article_${item.id}`} article={item} articleId={item.id} />
          ))}
        </div>
    )
  }
}

ArticlesList.propTypes = {
  loadArticles: PropTypes.func.isRequired,
  articles: PropTypes.array,
  articlesError: PropTypes.string
}