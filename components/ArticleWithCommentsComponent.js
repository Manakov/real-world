import React, { Component, PropTypes } from 'react'
import ArticleComponent from './ArticleComponent'

export default class ArticleWithCommentsComponent extends ArticleComponent {
  componentWillMount() {
    const {props} = this

    super.componentWillMount()

    props.loadArticleComments(props.articleId)
  }

  renderComments() {
    const {articleComments, articleCommentsError} = this.props

    if(articleCommentsError) {
      return <ErrorMessage message={articleCommentsError} />
    }

    if(!articleComments) { return <div className="loading">Loading comments...</div> }

    return (
      <div className="comments">
        {articleComments.map(item => (
            <div key={`comment_${item.id}`} className="comment">{item.body}</div>
        ))}
      </div>
    )
  }

  render() {
    return (
        <div class-name="article-with-comments">
          {super.render()}
          <br />
          Comments:
          {this.renderComments()}
        </div>
    )
  }
}

ArticleWithCommentsComponent.propTypes = {
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
  }),
  loadArticle: PropTypes.func,
  articleId: PropTypes.number.isRequired,
  articleError: PropTypes.string,
  loadArticleComments : PropTypes.func.isRequired,
  articleComments : PropTypes.array,
  articleCommentsError : PropTypes.string
}