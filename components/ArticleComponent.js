import React, { Component, PropTypes } from 'react'
import ErrorMessage from './ErrorMessage'

export default class ArticleComponent extends Component {
  componentWillMount() {
    const {props} = this

    if(typeof props.loadArticle === 'function') {
        props.loadArticle(props.articleId)
    }
  }

  render() {
    const {article, articleError} = this.props;

    if(articleError) {
        return <ErrorMessage message={articleError} />
    }

    if(!article) { return <div className="loading">Loading article...</div> }

    return (
        <div className="article" style={{ marginTop: 20 }}>
          <small className="author">{article.author}</small>
          <div className="title">{article.title}</div>
        </div>
    )
  }
}

ArticleComponent.propTypes = {
  article: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      author: PropTypes.string.isRequired
  }),
  loadArticle: PropTypes.func,
  articleId: PropTypes.number.isRequired,
  articleError: PropTypes.string
}