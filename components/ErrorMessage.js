import React, { Component, PropTypes } from 'react'

export default class ErrorMessage extends Component {
  render() {
    return (
        <p className="error" style={{ backgroundColor: '#e99', padding: 10 }}>
            Error: {this.props.message}
        </p>
    )
  }
}

ErrorMessage.propTypes = {
  message: PropTypes.string.isRequired
}