/* eslint-disable no-constant-condition */
import { take, put, call, fork, select, all } from 'redux-saga/effects'
import { api, history } from '../services'
import * as actions from '../actions'
//import { getUser, getRepo, getStarredByUser, getStargazersByRepo } from '../reducers/selectors'

// each entity defines 3 creators { request, success, failure }
const { targetLink, targetContent, articles, article, articleComments } = actions

/***************************** Subroutines ************************************/

// resuable fetch Subroutine
// entity :  user | repo | starred | stargazers
// apiFn  : api.fetchUser | api.fetchRepo | ...
// id     : login | fullName
// url    : next page url. If not provided will use pass id to apiFn
function* fetchEntity(entity, apiFn, id) {
  yield put( entity.request(id) )
  const {response, error} = yield call(apiFn, id)
  if(response)
    yield put( entity.success(id, response) )
  else
    yield put( entity.failure(id, error) )
}

// yeah! we can also bind Generators
export const fetchTargetLink = fetchEntity.bind(null, targetLink, api.fetchTargetLink)
export const fetchTargetContent = fetchEntity.bind(null, targetContent, api.fetchTargetContent)
export const fetchArticles = fetchEntity.bind(null, articles, api.fetchArticles)
export const fetchArticle = fetchEntity.bind(null, article, api.fetchArticle)
export const fetchArticleComments = fetchEntity.bind(null, articleComments, api.fetchArticleComments)

function* loadTargetLink(param) {
  yield call(fetchTargetLink, param)
}


function* loadTargetContent(param) {
  yield call(fetchTargetContent, param)
}

function* loadArticles() {
  yield call(fetchArticles)
}

function* loadArticle(id) {
  yield call(fetchArticle, id)
}

function* loadArticleComments(id) {
  yield call(fetchArticleComments, id)
}
/******************************************************************************/
/******************************* WATCHERS *************************************/
/******************************************************************************/
function* watchLoadTargetLink() {
  while(true) {
    const {pathname} = yield take(actions.LOAD_TARGET_LINK)
    yield fork(loadTargetLink, pathname, true)
  }
}

function* watchLoadArticles() {
  while(true) {
    yield take(actions.LOAD_ARTICLES)
    yield fork(loadArticles)
  }
}

function* watchLoadArticle() {
  while(true) {
    const {id} = yield take(actions.LOAD_ARTICLE)
    yield fork(loadArticle, id)
  }
}

function* watchLoadArticleComments() {
  while(true) {
    const {id} = yield take(actions.LOAD_ARTICLE_COMMENTS)
    yield fork(loadArticleComments, id)
  }
}

export default function* root() {
  yield all([
    fork(watchLoadTargetLink),
    fork(watchLoadArticles),
    fork(watchLoadArticle),
    fork(watchLoadArticleComments)
  ])
}
