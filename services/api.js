import 'isomorphic-fetch'

const API_ROOT = 'http://localhost:3001/'

function callApi(endpoint) {
  return fetch(endpoint)
      .then(response =>
          response.json().then(json => ({ json, response }))
      ).then(({ json, response }) => {
        if (!response.ok) {
          return Promise.reject(json)
        }

        return json
      })
      .then(
          response => ({response}),
          error => ({error: error.message || 'Something bad happened'})
  )
}


// api services
export const fetchTargetLink = pathname => callApi(`${API_ROOT}links?url=${pathname}`)
export const fetchArticles = () => callApi(`${API_ROOT}articles`)
export const fetchArticle = id => callApi(`${API_ROOT}articles/${id}`)
export const fetchArticleComments = id => callApi(`${API_ROOT}comments?articleId=${id}`)
