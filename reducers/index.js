import * as ActionTypes from '../actions'
import merge from 'lodash/object/merge'
import { combineReducers } from 'redux'

const initialState = {
  targetComponent: null,
  targetLinkError: null,
  articles:null,
  articlesError: null,
  contentId:null,
  article: null,
  articleError: null,
  articleComments: null,
  articleCommentsError: null
}

function articles(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.TARGET_LINK.REQUEST:
      return {
        ...state,
        targetLinkError: null
      }

    case ActionTypes.TARGET_LINK.FAILURE:
      return {
        ...state,
        targetLinkError: action.error
      }

    case ActionTypes.TARGET_LINK.SUCCESS:
      const data = action.response[0]

      return {
        ...state,
        targetComponent: data.component,
        articleId: data.id
      }

    case ActionTypes.ARTICLES.REQUEST:
      return {
        ...state,
        articlesError: null,
        articleError: null
      }

    case ActionTypes.ARTICLES.SUCCESS:
      return {
        ...state,
        articles: action.response
      }

    case ActionTypes.ARTICLES.FAILURE:
      return {
        ...state,
        articlesError: action.error
      }

    case ActionTypes.ARTICLE.REQUEST:
      return {
        ...state,
        articleError: null
      }

    case ActionTypes.ARTICLE.SUCCESS:
      return {
        ...state,
        article: action.response
      }

    case ActionTypes.ARTICLE.FAILURE:
      return {
        ...state,
        articleError: action.error
      }

    case ActionTypes.ARTICLE_COMMENTS.REQUEST:
      return {
        ...state,
        articleCommentsError: null
      }

    case ActionTypes.ARTICLE_COMMENTS.SUCCESS:
      return {
        ...state,
        articleComments: action.response
      }

    case ActionTypes.ARTICLE_COMMENTS.FAILURE:
      return {
        ...state,
        articleCommentsError: action.error
      }

    default:
      return state
  }
}

const rootReducer = combineReducers({
  articles
})

export default rootReducer
