
const REQUEST = 'REQUEST'
const SUCCESS = 'SUCCESS'
const FAILURE = 'FAILURE'

function createRequestTypes(base) {
  return [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
		acc[type] = `${base}_${type}`
		return acc
	}, {})
}

/*
export const USER = createRequestTypes('USER')
export const REPO = createRequestTypes('REPO')
export const STARRED = createRequestTypes('STARRED')
export const STARGAZERS = createRequestTypes('STARGAZERS')
*/
export const TARGET_LINK = createRequestTypes('TARGET_LINK')
export const ARTICLES = createRequestTypes('ARTICLES')
export const ARTICLE = createRequestTypes('ARTICLE')
export const ARTICLE_COMMENTS = createRequestTypes('ARTICLE_COMMENTS')

/*
export const UPDATE_ROUTER_STATE = 'UPDATE_ROUTER_STATE'
export const NAVIGATE =  'NAVIGATE'
export const LOAD_USER_PAGE = 'LOAD_USER_PAGE'
export const LOAD_REPO_PAGE = 'LOAD_REPO_PAGE'
export const LOAD_MORE_STARRED = 'LOAD_MORE_STARRED'
export const LOAD_MORE_STARGAZERS = 'LOAD_MORE_STARGAZERS'
export const RESET_ERROR_MESSAGE = 'RESET_ERROR_MESSAGE'
*/

export const LOAD_TARGET_LINK = 'LOAD_TARGET_LINK'
export const LOAD_ARTICLES = 'LOAD_ARTICLES'
export const LOAD_ARTICLE = 'LOAD_ARTICLE'
export const LOAD_ARTICLE_COMMENTS = 'LOAD_ARTICLE_COMMENTS'


function action(type, payload = {}) {
  return {type, ...payload}
}
/*
export const user = {
  request: login => action(USER[REQUEST], {login}),
  success: (login, response) => action(USER[SUCCESS], {login, response}),
  failure: (login, error) => action(USER[FAILURE], {login, error}),
}

export const repo = {
  request: fullName => action(REPO[REQUEST], {fullName}),
  success: (fullName, response) => action(REPO[SUCCESS], {fullName, response}),
  failure: (fullName, error) => action(REPO[FAILURE], {fullName, error}),
}

export const starred = {
  request: login => action(STARRED[REQUEST], {login}),
  success: (login, response) => action(STARRED[SUCCESS], {login, response}),
  failure: (login, error) => action(STARRED[FAILURE], {login, error}),
}

export const stargazers = {
  request: fullName => action(STARGAZERS[REQUEST], {fullName}),
  success: (fullName, response) => action(STARGAZERS[SUCCESS], {fullName, response}),
  failure: (fullName, error) => action(STARGAZERS[FAILURE], {fullName, error}),
}
*/

export const targetLink = {
  request: pathname => action(TARGET_LINK[REQUEST], {pathname}),
  success: (pathname, response) => action(TARGET_LINK[SUCCESS], {pathname, response}),
  failure: (pathname, error) => action(TARGET_LINK[FAILURE], {pathname, error})
}

export const articles = {
  request: () => action(ARTICLES[REQUEST], {}),
  success: (param, response) => action(ARTICLES[SUCCESS], {param, response}),
  failure: (param, error) => action(ARTICLES[FAILURE], {param, error})
}

export const article = {
  request: id => action(ARTICLE[REQUEST], {id}),
  success: (id, response) => action(ARTICLE[SUCCESS], {id, response}),
  failure: (id, error) => action(ARTICLE[FAILURE], {id, error})
}

export const articleComments = {
  request: param => action(ARTICLE_COMMENTS[REQUEST], {param}),
  success: (fullName, response) => action(ARTICLE_COMMENTS[SUCCESS], {fullName, response}),
  failure: (fullName, error) => action(ARTICLE_[FAILURE], {fullName, error})
}

/*
export const updateRouterState = state => action(UPDATE_ROUTER_STATE, {state})
export const navigate = pathname => action(NAVIGATE, {pathname})
export const loadUserPage = (login, requiredFields = []) => action(LOAD_USER_PAGE, {login, requiredFields})
export const loadRepoPage = (fullName, requiredFields = []) => action(LOAD_REPO_PAGE, {fullName, requiredFields})
export const loadMoreStarred = login => action(LOAD_MORE_STARRED, {login})
export const loadMoreStargazers = fullName => action(LOAD_MORE_STARGAZERS, {fullName})

export const resetErrorMessage = () => action(RESET_ERROR_MESSAGE)
*/

export const loadTargetLink = pathname => action(LOAD_TARGET_LINK, {pathname})
export const loadArticles = () => action(LOAD_ARTICLES)
export const loadArticle = (id) => action(LOAD_ARTICLE, {id})
export const loadArticleComments = (id) => action(LOAD_ARTICLE_COMMENTS, {id})