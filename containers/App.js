import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import {loadTargetLink, loadArticles, loadArticle, loadArticleComments } from '../actions'
//import Explore from '../components/Explore'
import { Link } from 'react-router'
import ArticlesList from '../components/ArticlesList'
import ArticleComponent from '../components/ArticleComponent'
import ArticleWithCommentsComponent from '../components/ArticleWithCommentsComponent'
import ErrorMessage from '../components/ErrorMessage'

class App extends Component {
  constructor(props) {
    super(props)
    this.targetContentComponents = {ArticlesList, ArticleComponent, ArticleWithCommentsComponent}
  }

  componentWillMount() {
    this.loadTargetLink(this.props.location.pathname)
  }

  componentWillReceiveProps(nextProps) {
    const currentPathname = this.props.location.pathname
    const nextPathname = nextProps.location.pathname

    if(currentPathname !== nextPathname) {
      this.loadTargetLink(nextPathname)
    }
  }

  loadTargetLink(pathname) {
    if(!pathname || pathname.length < 2) { return }

    this.props.loadTargetLink(pathname && pathname.substring(1))
  }

  renderNav() {
    return (
        <nav>
          <Link to="/articles">Articles</Link>&nbsp;&nbsp;
          <Link to="/articles/first">First Article</Link>&nbsp;&nbsp;
          <Link to="/articles/second/withComments">Second Article With Comments</Link>
        </nav>
    )
  }

  renderContentComponent() {
    const {props} = this
    const targetComponentName = props.targetComponent
    const targetComponent = targetComponentName && this.targetContentComponents[targetComponentName]

    if(props.targetLinkError) {
      return <ErrorMessage message={props.targetLinkError} />
    }

    if(!targetComponent || (typeof props.articleId === 'undefined')) { return null }

    return React.createElement(targetComponent, this.getPropsForContentComponent(targetComponentName))
  }

  getPropsForContentComponent (componentName) {
    const {loadArticles, articles, loadArticle, articlesError, article, articleId, articleError,
            loadArticleComments, articleComments, articleCommentsError} = this.props


    switch (componentName) {
      case 'ArticlesList':
        return {loadArticles, articles, articlesError}

      case 'ArticleComponent':
        return {loadArticle, article, articleId, articleError}

      case 'ArticleWithCommentsComponent':
        return {loadArticle, article, articleId, articleError, loadArticleComments, articleComments,
                articleCommentsError}

      default :
          return {}
    }
  }

  render() {
    return (
        <div>
          {this.renderNav()}
          <br />
          <br />
          {this.renderContentComponent()}
        </div>
    )
  }
}

App.propTypes = {
  // Injected by React Redux
  loadTargetLink: PropTypes.func.isRequired,
  targetLinkError: PropTypes.string,
  loadArticles: PropTypes.func.isRequired,
  loadArticle: PropTypes.func.isRequired,
  articles: PropTypes.array,
  articlesError: PropTypes.string,
  article: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
  }),
  articleId: PropTypes.number
}

function mapStateToProps(state) {
  const {articles} = state

  return {
    targetComponent: articles.targetComponent,
    targetLinkError: articles.targetLinkError,
    articles: articles.articles,
    articlesError: articles.articlesError,
    articleId: articles.articleId,
    article: articles.article,
    articleError: articles.articleError,
    articleComments: articles.articleComments,
    articleCommentsError: articles.articleCommentsError
  }
}

export default connect(mapStateToProps, {
  loadTargetLink,
  loadArticles,
  loadArticle,
  loadArticleComments
})(App)
